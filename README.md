GPX Course Profile Poltter
 
  It is originally made for Korea Randonneurs Course(http://www.korearandonneurs.kr/)
  Anyone can use this for his own GPX cours prfile visualization under GPLv2 license.
  
  Creaed course profile samples is here below.
  https://cdn.clien.net/web/api/file/F01/6809581/56f1fe94e44b6d.png?thumb=false
  
  
  Usage
  
  0. gpxcpp.py is main program. gpxplot is plotting module.
     When you run gpxcpp.py, the png file is created same directory that program is.
     
  1. Need GPX file of course, which contains elevation profile and waypoints.
  
  2. Elevation should aberve see see level, or the program does no plot that point.
  
  3. Waypoint has two kind. CP(Control Poind) and Summit.
     CP - Should start number eg: 1.Seoul, 2.NY...
     Summst - Prefix 's' eg: sChang La, sNamSan
	 
  4. Waypoint must placed within 20m radious from any course point, not course line.
     If some waypoint is missing in plot, verify the position of that waypoint.
	 
  5. This program is not user friendly, You have to modify the source(pgxcpp.py)
     to handle another GPX file. At least, change GPX file name.
  
  6. If you have any problems to install / run this program, don't ask me.
     Google it.
	 
  7. If you have any questions about Randonneurs, refer links below.
     http://www.korearandonneurs.kr/
	 http://www.audax-club-parisien.com/EN/index.php
	 https://rusa.org/
	 https://www.audax-japan.org/en/audax-japan/
	 http://app.audaxthailand.com/home
	 
	 Enjoy Riding !!!
  
  